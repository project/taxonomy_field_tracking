<?php

namespace Drupal\Tests\taxonomy_field_tracking\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test module.
 *
 * @group taxonomy field tracking
 */
class FormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'taxonomy_field_tracking',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * Test access to configuration page.
   */
  public function testCanAccessConfigPage() {
    $account = $this->drupalCreateUser([
      'access taxonomy field tracking settings',
      'access content',
    ]);

    $this->drupalLogin($account);
    $this->drupalGet('/admin/config/system/taxonomy_field_tracking');
    $this->assertSession()->pageTextContains('Taxonomy Field Tracking');
  }

}
