## CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION
------------

The Taxonomy Field Tracking (taxonomy_field_tracking) is a module that allows the override
of a view argument based on the preferences of a user.
Each time a user visits a node page a counter is set for each tid of a certain field.
This information is updated when the user continues navigating the website and
is used to generate a ranking of tids. These tids are then passed to the first argument of a certain view
allowing the contents of that view to adapt to the users navigation.

## REQUIREMENTS
------------

This module requires no modules outside of Drupal core. However, it was developed to use combined with
Views Extras module since Views Extra has an option to provide a contextual filter to a view based on session variables.


## INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


## CONFIGURATION
-------------

 * First navigate to

   Configuration » System » Taxonomy Field Tracking

   - Enable the tracking of preferences;

   - Configure the bundle, the field and the view to use (only one field and view per bundle are supported);

   - Select how many tids from the ranking should be sent to the view. Eg: if you select 1 then only the most watched tid will be sent;

   - Click Save Configuration.

Please note this module was designed only for simples cases, which means a simple view with no exposed filters and only one contextual filter

## MAINTAINERS
-----------

 * Current maintainers: [nsalves](https://www.drupal.org/u/nsalves)

This project has been sponsored by:
 * NTT DATA
    NTT DATA – a part of NTT Group – is a trusted global innovator of IT and business services headquartered in Tokyo.
    NTT is one of the largest IT services provider in the world and has 140,000 professionals, operating in more than 50 countries.
    NTT DATA supports clients in their digital development through a wide range of consulting and strategic advisory services, cutting-edge technologies, applications, infrastructure, modernization of IT and BPOs.
    We contribute with vast experience in all sectors of economic activity and have extensive knowledge of the locations in which we operate
