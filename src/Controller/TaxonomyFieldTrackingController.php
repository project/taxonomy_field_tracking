<?php

namespace Drupal\taxonomy_field_tracking\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller that receives data from node bundles.
 */
class TaxonomyFieldTrackingController extends ControllerBase {

  /**
   * Receive data from the client, parse it and store it for future use.
   */
  public function getData(Request $request) {

    if (!$this->isRequestSafe($request)) {
      return new JsonResponse("An error has occurred");
    }

    $json_string = $request->getContent();
    $decoded = Json::decode($json_string);
    // Node bundle is always last element of array.
    $node_bundle = end($decoded);
    // The other elements are the preferences.
    $preferences_slice = array_slice($decoded, 0, (count($decoded) - 1));
    $preferences = implode("+", $preferences_slice);

    // Sanitize taxonomy ids.
    $taxonomy_id_pattern = "/[^0-9+]/";
    $sanitized_preferences = preg_replace($taxonomy_id_pattern, "", $preferences);

    // Sanitize node bundle.
    $node_bundle_pattern = "/[^a-z]/i";
    $sanitized_node_bundle = preg_replace($node_bundle_pattern, "", $node_bundle);

    $_SESSION["taxonomy_field_tracking"][$sanitized_node_bundle] = $sanitized_preferences;
    return new JsonResponse($json_string);
  }

  /**
   * Checks if the request is an ajax request and if its from the same domain.
   */
  public function isRequestSafe(Request $request) {
    if ($request->isXmlHttpRequest() && $_SERVER['HTTP_ORIGIN'] === $request->getSchemeAndHttpHost()) {
      return TRUE;
    }
    return FALSE;
  }

}
