<?php

namespace Drupal\taxonomy_field_tracking\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Configure the Taxonomy Field Tracking settings.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_field_tracking_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['taxonomy_field_tracking.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get current configuration.
    $config = $this->config('taxonomy_field_tracking.settings');

    $form['active'] = [
      '#type' => 'radios',
      '#title' => $this->t('Preferences tracking'),
      '#default_value' => $config->get('active') ? $config->get('active') : 0,
      '#options' => [
        1 => $this->t('Enabled'),
        0 => $this->t('Disabled'),
      ],
      '#description' => $this->t('Selects if the tracking of the fields in the bundles below should be active or not.'),
    ];

    $form['entities'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Node Bundles to track'),
      '#default_value' => $config->get('entities'),
      '#description' => $this->t('Write your configuration separated by a "|" in the form content_type|field_name|view_id.'),
      '#required' => TRUE,
    ];

    $form['number_ids'] = [
      '#type' => 'number',
      '#default_value' => $config->get('number_ids'),
      '#description' => $this->t('This represent the number of most visited taxonomies that will be send to the configured views. e.g Selecting "1" will mean only the top visited taxonomy will be sent, selecting "2" will mean the two most visited taxonomies etc...'),
      '#title' => $this->t('Number of preferences to be used'),
    ];

    $form_state->setRebuild(TRUE);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('number_ids') <= 0) {
      $form_state->setErrorByName('number_ids', $this->t('The number of preferences to user should be greater than 0.'));
    }

    $node_configuration_list = explode(PHP_EOL, $form_state->getValue('entities'));

    // Check if the configured fields actually exist in the bundle.
    foreach ($node_configuration_list as $node_configuration) {
      $node_configuration = explode("|", $node_configuration);

      $field_storage = FieldStorageConfig::loadByName('node', $node_configuration[1]);
      if (empty($field_storage) || !in_array($node_configuration[0], $field_storage->getBundles())) {
        $form_state->setErrorByName('entities', $this->t('One or more fields do not exist for the configured bundles.'));
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('taxonomy_field_tracking.settings')
      ->set('entities', $form_state->getValue('entities'))
      ->set('number_ids', $form_state->getValue('number_ids'))
      ->set('active', $form_state->getValue('active'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
