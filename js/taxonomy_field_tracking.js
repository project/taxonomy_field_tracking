(function ($, Drupal) {

  Drupal.behaviors.taxonomy_field_tracking = {
    attach: function (context, settings) {
      if (context !== document) {
        return;
      }

      var data = drupalSettings.taxonomy_ids;
      //if the value to track is not filled for this node, no processing will be done
      if (data.length !== 0) {
        var amount = drupalSettings.number_ids
        var node_bundle = drupalSettings.node_bundle

        var preferences = [];
        for (var i = 0; i < data.length; i++) {
          preferences[i] = {
            "tid": data[i].target_id,
            "lastSeen": Date.now().toString(),
            "views": 1,
          }
        }
        //if there's already saved values we updated them
        if (localStorage.hasOwnProperty("Drupal." + node_bundle + ".preferences")) {
          currentStorage = localStorage.getItem("Drupal." + node_bundle + ".preferences")
          parsedStorage = JSON.parse(currentStorage);

          //iterate over preferences and updates the local storage values adding a new item if it does not exist
          for (let key in preferences) {
            found = false
            for (let key2 in parsedStorage) {
              if (parsedStorage[key2].tid === preferences[key].tid) {
                parsedStorage[key2].lastSeen = preferences[key].lastSeen
                parsedStorage[key2].views = parsedStorage[key2].views + preferences[key].views
                found = true
              }
            }
            if (!found) {
              parsedStorage[Object.keys(parsedStorage).length] = preferences[key]
            }
          }
          // set update values
          sortedArray = sortArray(parsedStorage)
          //only send the amount of values defined in the configuration form
          sortedArray.length = amount
          localStorage.setItem("Drupal." + node_bundle + ".preferences", JSON.stringify(parsedStorage));
          sendData(sortedArray)
        } else {
          //if its the first time saving values we set the local storage
          localStorage.setItem("Drupal." + node_bundle + ".preferences", JSON.stringify(preferences));
          sendData(preferences)
        }
      }

      //aux functions

      function sendData(array) {
        data = []
        for (var i = 0; i < array.length; i++) {
          data[i] = array[i].tid
        }
        data.push(node_bundle)
        jQuery.ajax({
          url: '/taxonomy_field_tracking_controller',
          type: "POST",
          data: JSON.stringify(data),
          dataType: "json",
          error: function (result) {
            console.log("An error has occurred");
          },
        });

      }

      function sortArray(array) {
        var sortedArray = array.slice(0);
        sortedArray.sort(function (a, b) {
          return b.views - a.views;
        });
        return sortedArray
      }

    }
  };
})(jQuery, Drupal);
